<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="68"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;The project provides a template for Aurora OS applications.&lt;/p&gt;
&lt;p&gt;The main purpose is to clearly demonstrate almost minimal source code to get a correct and extensible application.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="83"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (C) 2022 Open Mobile Platform LLC&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
  &lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
  &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
  &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="73"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
</context>
<context>
    <name>AutopilotSelector</name>
    <message>
        <location filename="../qml/selectors/AutopilotSelector.qml" line="88"/>
        <source>Autopilot</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CarSelector</name>
    <message>
        <location filename="../qml/selectors/CarSelector.qml" line="56"/>
        <source>Select your car</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/selectors/CarSelector.qml" line="112"/>
        <source>0-60 mph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/selectors/CarSelector.qml" line="133"/>
        <source>Top Speed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="47"/>
        <source>Tesla Model Demo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DummyModels</name>
    <message>
        <location filename="../qml/DummyModels.qml" line="9"/>
        <source>Included</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExteriorSelector</name>
    <message>
        <location filename="../qml/selectors/ExteriorSelector.qml" line="55"/>
        <source>Select color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/selectors/ExteriorSelector.qml" line="107"/>
        <source>20’’ Performance Wheels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/selectors/ExteriorSelector.qml" line="113"/>
        <source>Carbon fiber spoiler</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InteriorSelector</name>
    <message>
        <location filename="../qml/selectors/InteriorSelector.qml" line="88"/>
        <source>Select interior</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="102"/>
        <source>ORDER NOW</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsPage</name>
    <message>
        <location filename="../qml/pages/OptionsPage.qml" line="248"/>
        <source>NEXT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SummaryPage</name>
    <message>
        <location filename="../qml/pages/SummaryPage.qml" line="114"/>
        <source>Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SummaryPage.qml" line="173"/>
        <source>Pay</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
