pragma Singleton
import QtQuick 2.0
import QtQml.Models 2.2

QtObject {

    function formatMoney(value) {
        if (value == 0) {
            return qsTr("Included")
        }
        return "$"+parseFloat(value).toLocaleString(Qt.locale("en_US"), 'f', 0)
    }

    property ListModel optionsModel: ListModel {
        ListElement { name: "Car" }
        ListElement { name: "Exterior" }
        ListElement { name: "Interior" }
        ListElement { name: "Autopilot" }
    }

    property ListModel carOptionsModel: ListModel {
        ListElement {
            name: "Performance"
            price: "55700"
            description: "Tesla All-Wheel Drive has two independent motors. Unlike traditional all-wheel drive systems, these two motors digitally control torque to the front and rear wheels—for far better handling and traction control."
            topSpeed: "150mph"
            sixtyMilesTime: "3.5s"
        }

        ListElement {
            name: "Long Range"
            price: "46700"
            description: "Some boring Long Range description"
            topSpeed: "130mph"
            sixtyMilesTime: "5s"
        }
    }

    property ListModel exteriorColorsModel: ListModel {
        ListElement { color: "black"; name: "Fancy Black"; price: "2000"}
        ListElement { color: "grey"; name: "Space Gray"; price: "1700"}
        ListElement { color: "blue"; name: "Sad Blue"; price: "1800"}
        ListElement { color: "white"; name: "Pearl White Multi-Coat"; price: "1900"}
        ListElement { color: "red"; name: "Deafening Red"; price: "2000"}
    }

    property ListModel interiorColorsModel: ListModel {
        ListElement { color: "white"; name: "Black and White"; price: "1000"}
        ListElement { color: "black"; name: "All Black"; price: "0"}

    }

    property ListModel autopilotModel: ListModel {
        ListElement { name: "Full Self-Driving"; price: "5000"
            description: "Atomatic driving from highway on-ramp to off-ramp including interchanges and overtaking slower cars."
        }
        ListElement { name: "Autopilot"; price: "3000"
            description: "Some boring autopilot description"
        }

    }

    property ListModel dummyModel: ListModel {
        ListElement {
            name: "Model X"
            range: "300 mi"
            motor: "AWD"
            acceleration: "0-60 mph in 3.5s"
            topSpeed: "up to 150 mph"
            car: "modelXCar"
        }

        ListElement {
            name: "Model Y"
            range: "300 mi"
            motor: "AWD"
            acceleration: "0-60 mph in 3.5s"
            topSpeed: "up to 150 mph"
        }

        ListElement {
            name: "Roadster"
            range: "300 mi"
            motor: "AWD"
            acceleration: "0-60 mph in 3.5s"
            topSpeed: "up to 150 mph"
        }
    }
}
