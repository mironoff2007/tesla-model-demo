/*******************************************************************************
**
** Copyright (C) 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Aurora OS Tesla Model Demo project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

import DummyModels 1.0

import "../components"

SelectorItem {
    id: exteriorSelector
    anchors.fill: parent

    Column {
        anchors.fill: parent
        anchors.margins: Theme.horizontalPageMargin

        Label {
            text: qsTr("Select color")
            font.pixelSize: Theme.fontSizeLarge
            color: "#A4B0BC"
        }

        Image {
            width: parent.width
            fillMode: Image.PreserveAspectFit
            source: Qt.resolvedUrl("../resources/"+colorsRow.modelGet(colorsRow.currentIndex).color+"-car.png")
            scale: 1.4
        }

        Row {
            width: parent.width
            height: Theme.itemSizeMedium

            Column {
                id: colorDetailColumn
                Label {
                    text: colorsRow.modelGet(colorsRow.currentIndex).name
                    font.pixelSize: Theme.fontSizeLarge
                    color: "black"
                }
                Label {
                    text: DummyModels.formatMoney(colorsRow.modelGet(colorsRow.currentIndex).price)
                    font.pixelSize: Theme.fontSizeLarge
                    color: "red"
                }
            }
        }

        ColorsRow {
            id: colorsRow
            model: DummyModels.exteriorColorsModel
            onClickedSelect: {
                exteriorSelector.money = modelGet(currentIndex).price
            }
        }

        Item {
            width: parent.width
            height: Theme.itemSizeSmall

            Rectangle {
                anchors.verticalCenter: parent.verticalCenter
                width: parent.width
                height: Theme.dp(2)
                color: "#CFD3D9"
            }
        }

        Label {
            text: qsTr("20’’ Performance Wheels")
            font.pixelSize: Theme.fontSizeExtraSmall
            color: "black"
        }

        Label {
            text: qsTr("Carbon fiber spoiler")
            font.pixelSize: Theme.fontSizeExtraSmall
            color: "black"
        }
    }
    Component.onCompleted: {
        exteriorSelector.money = colorsRow.modelGet(colorsRow.currentIndex).price
    }
}

