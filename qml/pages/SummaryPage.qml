/*******************************************************************************
**
** Copyright (C) 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Aurora OS Application Template project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

import DummyModels 1.0

Page {
    id: summaryPage
    objectName: "mainPage"
    allowedOrientations: Orientation.Portrait

    property real finalPrice

    background: Rectangle {
        color: "black"
    }

    Image {
        anchors.centerIn: parent
        source: Qt.resolvedUrl("../resources/summary-bg.png")
        scale: 1.8
        anchors.verticalCenterOffset: -height * 0.2
        rotation: 0
    }

    SilicaListView {
        visible: false
        anchors.fill: parent
        model: 5
        orientation: ListView.Horizontal
        delegate: BackgroundItem {
            width: Theme.itemSizeLarge
            height: width
            Label {
                anchors.centerIn: parent
                text: index
            }
        }
        footer: Rectangle {
            width: summaryPage.width
            height: Theme.itemSizeHuge
        }
    }

    Item {
        id: pageHeader
        anchors.top: parent.top
        width: parent.width
        height: Theme.itemSizeLarge
        Image {
            source: Qt.resolvedUrl("../resources/tesla-logo-dark.png")
            anchors.centerIn: parent
            scale: 1.2
        }
    }

    Item {
        id: pageFooter
        anchors.bottom: parent.bottom
        width: parent.width
        height: Theme.itemSizeHuge * 2

        Rectangle {
            width: parent.width
            anchors.top: parent.top
            height: parent.height + radius
            color: "black"
            radius: Theme.iconSizeSmall
        }

        Column {
            anchors.margins: Theme.horizontalPageMargin * 1.5
            anchors.fill: parent

            spacing: Theme.paddingMedium

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Summary")
                font.pixelSize: Theme.fontSizeLarge
                color: "#A4B0BC"
            }

            Item {
                width: parent.width
                height: Theme.paddingSmall
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Your " + "Model Y"
                font.bold: true
                font.pixelSize: Theme.fontSizeHuge
                color: "white"
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: DummyModels.formatMoney(summaryPage.finalPrice)
                font.pixelSize: Theme.fontSizeExtraLarge
                color: "white"
            }

        }

        BackgroundItem {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter

            width: parent.width / 2
            contentItem.radius: height
            contentItem.border.width: Theme.dp(2)
            contentItem.border.color: "red"
            highlightedColor: "red"

            Row {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }
                height: parent.height
                width: childrenRect.width

                Icon {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: -height * 0.1
                    width: height
                    height: Theme.fontSizeExtraLargeBase
                    source: Qt.resolvedUrl("../resources/apple-logo-light.png")
                    color: "white"
                    highlightColor: "white"
                }


                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Pay")
                    font.bold: true
                    font.pixelSize: Theme.fontSizeExtraLarge
                    color: "white"
                }
            }



            onClicked: {
                console.log("clicked pay")
                Qt.openUrlExternally("https://www.tesla.com/")

            }
        }
    }

    SilicaGridView {
        visible: false
        anchors.fill: parent
        cellWidth: summaryPage.width
        cellHeight: Theme.itemSizeHuge
        contentWidth: summaryPage.width
        contentHeight: Theme.itemSizeHuge + 1
        header: PageHeader {
            Rectangle {
                Rectangle {
                    anchors.top: parent.top
                    width: parent.width
                    height: parent.height / 2
                }

                anchors.fill: parent
                radius: Theme.iconSizeMedium
            }

        }

        model: 8

        delegate: BackgroundItem {
            width: summaryPage.width
            height: Theme.itemSizeHuge
            Label {
                anchors.centerIn: parent
                text: index
            }
        }

        footer: Rectangle {
            width: GridView.view.width
            height: Theme.itemSizeHuge
            radius: Theme.iconSizeMedium
            Rectangle {
                anchors.bottom: parent.bottom
                width: parent.width
                height: parent.height / 2
            }
        }
    }


}
