/*******************************************************************************
**
** Copyright (C) 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Aurora OS Tesla Model Demo project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

import DummyModels 1.0

Row {
    id: colorsRow
    width: parent.width
    spacing: Theme.paddingLarge

    height: Theme.itemSizeSmall

    property int currentIndex: 0
    property alias count: colorRepeater.count
    property alias model: colorRepeater.model
    signal clickedSelect(int index)

    function modelGet(index) {
        return colorRepeater.model.get(index)
    }

    function selectColor(selectedIndex) {
        for (var i = 0; i < colorRepeater.count; i++) {
            if (i === selectedIndex) {
                colorRepeater.itemAt(i).highlighted = true
                continue
            }
            colorRepeater.itemAt(i).highlighted = false
        }
        currentIndex = selectedIndex
    }

    Component.onCompleted: colorsRow.selectColor(0)

    Repeater {
        id: colorRepeater
        anchors.verticalCenter: parent.verticalCenter

        BackgroundItem {
            id: colorItem
            width: height
            height: Theme.itemSizeSmall
            highlightedColor: "transparent"


            Rectangle {
                id: colorRectangle
                anchors.fill: parent
                radius: height
                border.width: highlighted ? Theme.dp(4) : 0
                border.color: "red"
                color: model.color
                gradient: Gradient {
                     GradientStop { position: 0.0; color: colorRectangle.color }
                     GradientStop { position: 1.0; color: Qt.darker(colorRectangle.color, 3.0) }
                }
            }

            onClicked: {
                colorsRow.selectColor(index)
                clickedSelect(index)
            }
        }

    }
}
